import React, {useState, useEffect, useRef} from 'react';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {SaveIcon, CloseIcon, DeleteIcon, ArrowUpIcon, AppHeader, TitleText, OperationText, TextHeaderBox, TextInput, TextGroup, SericeStatusContainer, ServerContainer, ServerConent} from './styles'

const SERVICE_STATUS_GET = "http://localhost:8080/api/v1/service"
const SERVICE_DELETE = "http://localhost:8080/api/v1/service/delete/"
const SERVICE_REGISTER = "http://localhost:8080/api/v1/service/register/"
const SERVICE_UPDATE = "http://localhost:8080/api/v1/service/update/"
const REGISTER_TEMPLATE_TEXT = "Register Service"
const UPDATE_TEMPLATE_TEXT = "Update Service: "
const TOAST_SERVICE_DELETE = "Service deleted"
const TOAST_SERVICE_REGISTER = "Service registered"
const TOAST_SERVICE_UPDATE = "Service updated"
const TOAST_REFRESH = "Content refreshed"
const REFRESH_TIMER = 30000

/**
 * Component that renders the bulk of the UI needed for the application
 * 
 * @returns JSX which shows the service status
 */
const ServiceStatus = () => {

  const [services, setServices] = useState([]);
  const [activeService, setActiveService] = useState([]);
  const [operationText, setOperationText] = useState(REGISTER_TEMPLATE_TEXT);
  const [isUpdate, setIsUpdate] = useState(false);
  const nameInput = useRef(null);
  const urlInput = useRef(null);

  /**
   * Function which gets all the services via the GET API
   */
  const fetchServiceStatus = () =>{
    axios.get(SERVICE_STATUS_GET).then(res => {
    setServices(res.data);
  }).catch(error => {
    toast.error(error.response.data);
  });
  };

  /**
   * Function to delete the service selected, calls the DELETE API
   * 
   * @param {ServiceEntity} service  Service to be deleted
   */
  const deleteService = (service) =>{
    axios.delete(SERVICE_DELETE + service.name).then(res => {
    fetchServiceStatus();
    toast.success(TOAST_SERVICE_DELETE)
  }).catch(error => {
    toast.error(error.response.data);
  });
  };

  /**
   * Register a new service of a given name and url, calls the POST API
   * 
   * @param {String} name 
   * @param {String} url 
   */
  const registerService = (name, url) =>{
    const service = { name: name, url:url };
    axios.post(SERVICE_REGISTER, service).then(res => {
    fetchServiceStatus();
    toast.success(TOAST_SERVICE_REGISTER);
  }).catch(error => {
    toast.error(error.response.data);
  })
  };

  /**
   * Updates the selected service with the user desired name and/or URL, calls the PUT API
   * 
   * @param {String} name 
   * @param {String} url 
   */
  const updateService = (name, url) =>{
    const service = { name: name, url:url };
    axios.put(SERVICE_UPDATE + activeService.name, service).then(res => {
    fetchServiceStatus();
    toast.success(TOAST_SERVICE_UPDATE);
  }).catch(error => {
    toast.error(error.response.data);
  })
  };

  /**
   * Decides whether the PUT or POST API needs to be called based on if a service is currently active or not
   * 
   * @param {String} name 
   * @param {String} url 
   */
  const updateOrRegisterService = (name, url) => {
    if(isUpdate) {
      updateService(name, url);
    } else {
      registerService(name, url);
    }
  }

  /**
   * Clears the currently selected service and text input fields
   */
  const clearActiveSelection = () => {
    setIsUpdate(false);
    setActiveService([]);
    setOperationText(REGISTER_TEMPLATE_TEXT);
    nameInput.current.value = null;
    urlInput.current.value = null;
  }

  /**
   * Sets the activeService state to the currently selected service
   * 
   * @param {ServiceEntity} service 
   */
  const setSelectedService = (service) => {
    setActiveService(service);
    setIsUpdate(true);
    setOperationText(UPDATE_TEMPLATE_TEXT + service.name);
    nameInput.current.value = service.name;
    urlInput.current.value = service.url;
  };

  /**
   * The useEffect hook responsible for fetching data from the database periodically
   * and refresh the data in case of change
   */
  useEffect(() => {
    fetchServiceStatus();
    const timer = setInterval(() => {
      fetchServiceStatus();
      toast.info(TOAST_REFRESH);
    }, REFRESH_TIMER);
    return () => clearInterval(timer);
},[]);


return (
<SericeStatusContainer>
<TitleText>Service Poller</TitleText>
<TextHeaderBox status={activeService.status}>
<OperationText>{operationText}</OperationText>
  <TextGroup>
    <TextInput ref = {nameInput} type = "text" name = "name" placeholder="name" />
    <TextInput ref = {urlInput} type = "text" name = "url" placeholder="url" size = "L" />
  </TextGroup>
  <SaveIcon onClick={ () => updateOrRegisterService(nameInput.current.value, urlInput.current.value) }/>
  <CloseIcon isActive={isUpdate.toString()} onClick={ () => clearActiveSelection() }/>
</TextHeaderBox>
{services.map((service) => {
  return (
    <ServerContainer key= {service.id} status={service.status} activeService={activeService.id === service.id}>
    <ArrowUpIcon status={service.status}/>
    <ServerConent onClick={ () => setSelectedService(service) }>
      <h2>{service.name}</h2>
      <p>{service.url}</p>
    </ServerConent>
    <DeleteIcon isActive={(activeService.id === service.id).toString()} onClick={ () => deleteService(service) }></DeleteIcon>
    </ServerContainer>
  );
})}
</SericeStatusContainer>)
};


/**
 * The root component which encapsulates ServiceStatus component
 * 
 * @returns The JSX for the root render
 */
function App() {

  return (
      <AppHeader>
      <ServiceStatus/>
      <ToastContainer 
      theme="dark"
      position="bottom-right"
      autoClose={3000}
      hideProgressBar={false}
      newestOnTop={false}
      closeOnClick
      rtl={false}
      pauseOnFocusLoss={false}
      draggable
      pauseOnHover/>
      </AppHeader>
  );
}

export default App;
