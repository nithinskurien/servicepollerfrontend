# Service Poller Frontend
The React app is meant to fetch the status(UP/DOWN) of a service that is obtained from a database. The APP interacts with API endpoints to GET, POST, PUT and DELETE the service information.
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

![Service Poller](./img/main.png)

## Dependency
* [Service Poller Backend](https://bitbucket.org/nithinskurien/servicepollerbackend/src/master/) Needed as the Java backend setups the API endpoints used by the React app
* [styled-components](https://styled-components.com/) Enables users to make css elements into react components
* [axios](https://github.com/axios/axios-components.com/) Used for getting async requests
* [toastify](https://github.com/fkhadra/react-toastify#readme) Toast messages

## Instructions
The Java backend needs to be up and running for the app to work or the API endpoints can be changed in App.js file to point to new APIs. The API endpoints used are:

* GET: http://localhost:8080/api/v1/service/
* DELETE: http://localhost:8080/api/v1/service/delete/
* POST: http://localhost:8080/api/v1/service/register/
* PUT: http://localhost:8080/api/v1/service/update/

Please read the readme for the backend repo to set it up before continuing [Service Poller Backend](https://bitbucket.org/nithinskurien/servicepollerbackend/src/master/). Once the backend is up and the frontend project is cloned, we can open the frontend project in an IDE and in the console of the IDE one can run (node.js is required):

### `npm install`

This will install all the dependencies, once the dependecies are installed

### `npm start`

Runs the app in the development mode.\

Open [http://localhost:3000](http://localhost:3000) to view it in the browser (if default settings are not been changed).

## Features
![Service Poller Register](./img/register.png)
![Service Poller Update](./img/update.png)

The app has the option to VIEW, ADD, MODIFY and DELETE services. The app will show the list of services. The app has text fields that can be used to enter service name and URL. By default the app is in service registering mode where we can enter an name and URL and press save to save it. The services can also be deleted by using the trashcan icon next to each service. By selecting a service we highlight it for edit mode. In edit mode the delete function will be disabled and the user can decide the new name/URL and press save to update the service entry. To escape from the edit mode or to clear the text input the "X" can be pressed which brings the text fields and save button to the register service functionality. The App refreshes it's content at a fixed interval.

