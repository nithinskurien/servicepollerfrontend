import styled from 'styled-components';
import { MdSave, MdDelete, MdClose, MdArrowUpward } from 'react-icons/md';

/**
 * Styled-Components that are used in the app.js components
 */

const AppHeader = styled.div`
  background-color: #282c34;
  min-height: 100vh;

`
const SericeStatusContainer = styled.div`
  display: flex;
  flex-direction: column;
  display: flex;
  flex-direction: column;
  align-items: center;
  color: white;
  min-width: 800px;

`

const TitleText = styled.h1`
    opacity: 80%;
`

const OperationText = styled.h3`
    position:absolute;
    top: -50px;
    left: 50px;
    opacity: 50%;
`

const TextHeaderBox = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  width: 60vw;
  height: 100px;
  min-width: 860px;
  border-radius: 50px;
  backdrop-filter: saturate(20%);
  backdrop-filter: brightness(0.4);
  background-color: ${({ status }) => ((status === "UP") ? "#348E40" : (status === "DOWN" || status === "INVALID") ? "#C01825": "#20232a")};
  margin: 50px 0px 50px 0px;

`
const TextGroup = styled.div`
  display:flex;
  width: 55vw;
  min-width: 730px;
`

const TextInput = styled.input`
  margin: 10px 15px 10px 10px;
  border-radius: 40px;
  flex-grow: ${({ size }) => (size === "L" ? 2 : 0)};
  background-color: #262a32;
  padding: 10px;
  color: white;
  font-size: 1.5rem;
  min-width: 250px;

`
const ServerConent = styled.div`
  width: 55vw;
  min-width: 670px;
  cursor: pointer;
`
const DeleteIcon = styled(MdDelete)`
  color: white;
  width: 32px;
  height: 32px;
  cursor: pointer;
  margin: 16px;
  visibility: ${({ isActive }) => (isActive === "false") ? "visible" : "hidden"};
`

const SaveIcon = styled(MdSave)`
  color: white;
  width: 32px;
  height: 32px;
  cursor: pointer;
  margin: 16px;
`

const ArrowUpIcon = styled(MdArrowUpward)`
  color: white;
  width: 32px;
  height: 32px;
  cursor: pointer;
  margin: 16px;
  transform: rotate(${({ status }) => (status === "UP" ? "0deg" : "180deg")});
`

const CloseIcon = styled(MdClose)`
  color: white;
  width: 32px;
  height: 32px;
  cursor: pointer;
  margin: 16px;

`
const ServerContainer = styled.div`
  background-color: ${({ status }) => (status === "UP" ? "#348E40" : "#C01825")};
  filter: opacity(${({ activeService }) => (activeService? "100%" : "70%")});
  transform: scale(${({ activeService }) => (activeService? "1" : "0.95")});
  text-align: center;
  align-items: center;
  display: flex;
  justify-content: center;
  margin: 2vh;
  width: 60vw;
  height: 100px;
  min-width: 810px;
  border-radius: 50px;
`

export {SaveIcon, CloseIcon, ArrowUpIcon, DeleteIcon, AppHeader, TitleText, OperationText, TextHeaderBox, TextInput, TextGroup, SericeStatusContainer, ServerContainer, ServerConent};